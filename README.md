# OdooClient
Client in charge of generating a connection with Odoo service in order to show the differents products, this will be upgrated to obtain more differents types of data.

## Installation
Use package manager [Composer](https://getcomposer.org/) to install OdooClient
```bash
composer require dsarhoya/dsy-odoo-client
```

## Usage And Options
to use this package you need to use at least one of the following options
```php
<?php
$options = [
    'url' => 'https://some-web.odoo.com/',
    'db' => 'some-database',
    'user' => 'username',
    'pass' => 'password',
];
```
and generate the client to get the products
```php
<?php
$client = new OdooClient($options);
$client->product->getProducts([
    'limit' => 10
]);
```
this will always return an array whit the product.
the options for the `getProducts()` function are.-


Option Name | Value | Description 
--- | --- | --- 
is_sale_ok | `boolean` | helps to get the products that are available for sale
is_purchase_ok | `boolean` | helps to get the products that are available for purchase
price_lower_than | `integer` | get the products that are lower price than the value given
price_higher_than | `integer` | get the products that are higher price than the value given
quantity_lower_than | `integer` | get the products that have lower quantity than the value given
quantity_higher_than | `integer` | get the products that have higher quantity than the value given
limit | `integer` | limit the number of products to obtain in the query
offset | `integer` | helps to obtain diferetens products jumping from the value given