<?php

namespace Dsarhoya\OdooClient\Elements;

/**
 * Stock.
 */
class Stock extends AbstractElement
{
    public function getStock($id)
    {
        $this->authenticate();

        $result = $this->objectsClient->call(
            'execute_kw',
            [
                $this->options['db'],
                $this->user_id,
                $this->options['pass'],
                'stock.quant',
                'read',
                [[$id]],
            ]);

        if (is_array($result)) {
            return array_shift($result);
        }

        return null;
    }

    public function getStocks(array $options = [])
    {
        $this->authenticate();

        $options = array_replace([
            'offset' => 0,
            'limit' => 10,
            'stock_id' => null,
            'location_id' => null,
            'product_id' => null,
            'product_uom_id' => null,
        ], $options);

        $filters = [];

        if (null !== $options['location_id']) {
            $filters[] = ['location_id', '=', $options['location_id']];
        }

        if (null !== $options['product_id']) {
            $filters[] = ['product_id', '=', $options['product_id']];
        }

        if (null !== $options['product_uom_id']) {
            $filters[] = ['product_uom_id', '=', $options['product_uom_id']];
        }

        if (null !== $options['stock_id']) {
            $filters[] = ['id', '=', $options['stock_id']];
        }

        $result = $this->objectsClient->call(
            'execute_kw',
            [
                $this->options['db'],
                $this->user_id,
                $this->options['pass'],
                'stock.quant',
                'search_read',
                [$filters],
                [
                    'limit' => $options['limit'],
                    'offset' => $options['offset'],
                ],
            ]);

        return $result;
    }
}
