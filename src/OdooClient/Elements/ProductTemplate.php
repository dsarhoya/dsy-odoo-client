<?php

namespace Dsarhoya\OdooClient\Elements;

/**
 * ProductTemplate.
 */
class ProductTemplate extends AbstractElement
{
    public function getProductTemplates(array $options = [])
    {
        $this->authenticate();

        $options = array_replace([
            'offset' => 0,
            'limit' => 10,
            'location_id' => null,
        ], $options);

        $result = $this->objectsClient->call(
            'execute_kw',
            [
                $this->options['db'],
                $this->user_id,
                $this->options['pass'],
                'product.template',
                'search_read',
                [],
                [
                    'fields' => ['id', 'name', 'description', 'categ_id', 'list_price', 'company_id', 'active', 'default_code',
                    'price', 'lst_price', 'standard_price', 'product_variant_id', 'qty_available', 'display_name'],
                    'limit' => $options['limit'],
                    'offset' => $options['offset'],
                ],
            ]);

        return $result;
    }
}
