<?php

namespace Dsarhoya\OdooClient\Elements;

use Dsarhoya\OdooClient\CustomFXmlRpcClient;

class AbstractElement
{
    /**
     * @var Client
     */
    protected $authClient;

    /**
     * @var Client
     */
    protected $objectsClient;

    /**
     * @var int
     */
    protected $userId;

    /** @var array */
    protected $options;

    public function __construct(array $options)
    {
        $this->options = $options;
        $this->authClient = new CustomFXmlRpcClient($options['url'].'xmlrpc/2/common');
        $this->objectsClient = new CustomFXmlRpcClient($options['url'].'xmlrpc/2/object');
    }

    public function authenticate()
    {
        if (null != $this->userId) {
            return $this->authClient;
        }
        $this->user_id = $this->authClient->call(
            'authenticate',
            [
                $this->options['db'],
                $this->options['user'],
                $this->options['pass'],
                [],
            ]);
    }

    public function getObjectsClient()
    {
        return $this->objectsClient;
    }

    public function getAuthClient()
    {
        return $this->authClient;
    }
}
