<?php

namespace Dsarhoya\OdooClient\Elements;

/**
 * ProductVariant
 */
class ProductVariant extends AbstractElement
{
    public function getProductVariants(array $options = [])
    {
        $this->authenticate();

        $options = array_replace([
            'offset' => 0,
            'limit' => 10,
            'location_id' => null,
        ], $options);

        $result = $this->objectsClient->call(
            'execute_kw',
            [
                $this->options['db'],
                $this->user_id,
                $this->options['pass'],
                'product.product',
                'search_read',
                [],
                [
                    'limit' => $options['limit'],
                    'offset' => $options['offset'],
                ],
            ]);

        return $result;
    }
}
