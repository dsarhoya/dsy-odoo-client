<?php

namespace Dsarhoya\OdooClient\Elements;

/**
 * StockLocation.
 */
class StockLocation extends AbstractElement
{
    public function getStockLocations(array $options = [])
    {
        $this->authenticate();

        $options = array_replace([
            'offset' => 0,
            'limit' => 10,
        ], $options);

        $fields = [
            'id',
        ];

        $result = $this->objectsClient->call(
            'execute_kw',
            [
                $this->options['db'],
                $this->user_id,
                $this->options['pass'],
                'stock.location',
                'search_read',
                [
                ],
                [
                    'limit' => $options['limit'],
                    'offset' => $options['offset'],
                ],
            ]);

        return $result;
    }
}
