<?php

namespace Dsarhoya\OdooClient;

/**
 * @var Client
 */
class OdooClient
{
    /**
     * @var Elements\ProductTemplate;
     */
    public $productTemplates;

    /**
     * @var Elements\ProductVariant;
     */
    public $productVariants;

    /**
     * @var Elements\Stock;
     */
    public $stocks;

    /**
     * @var Elements\StockLocation;
     */
    public $stockLocations;

    /**
     * @var array options to use on diferents clases
     */
    private $options;

    /** require values.
     * @param array $options require
     *                       string base_url, string username, string password, string database
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;

        $this->productVariants = new Elements\ProductVariant($this->options);
        $this->productTemplates = new Elements\ProductTemplate($this->options);
        $this->stocks = new Elements\Stock($this->options);
        $this->stockLocations = new Elements\StockLocation($this->options);

        /*$options = [
            'search' => 'table.model',
            'search_options' => ['is_sale_ok', '=', true],
        ];*/
        /* this always returns a product */
        /*$this->product->getProducts([
            'is_sale_ok' => true,
            'is_purchase_ok' => true,
            'order' => 'ASC/DESC',
            'specific_product' => 'id/SKU',
            'limit' => 20,
            'offset' => 10,
        ]);*/
    }
}
