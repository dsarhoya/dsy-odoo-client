<?php

namespace Dsarhoya\OdooClient;

use fXmlRpc\Exception\InvalidArgumentException;
use fXmlRpc\Parser\ParserInterface;
use fXmlRpc\Parser\XmlReaderParser;
use fXmlRpc\Serializer\SerializerInterface;
use fXmlRpc\Serializer\XmlWriterSerializer;
use fXmlRpc\Transport\HttpAdapterTransport;
use fXmlRpc\Transport\TransportInterface;
use fXmlRpc\ClientInterface;
use fXmlRpc\MulticallBuilder;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;

final class CustomFXmlRpcClient implements ClientInterface
{
    /** @var string */
    private $uri;

    /** @var TransportInterface */
    private $transport;

    /** @var Parser\ParserInterface */
    private $parser;

    /** @var Serializer\SerializerInterface */
    private $serializer;

    /** @var array */
    private $prependParams = [];

    /** @var array */
    private $appendParams = [];

    /**
     * Create new client instance
     *
     * If no specific transport, parser or serializer is passed, default implementations
     * are used.
     *
     * @param string                         $uri
     * @param TransportInterface             $transport
     * @param Parser\ParserInterface         $parser
     * @param Serializer\SerializerInterface $serializer
     */
    public function __construct(
        $uri = null,
        TransportInterface $transport = null,
        ParserInterface $parser = null,
        SerializerInterface $serializer = null
    )
    {
        $this->uri = $uri;
        $this->transport = $transport
            ?: new HttpAdapterTransport(MessageFactoryDiscovery::find(), HttpClientDiscovery::find());
        $this->parser = $parser ?: new XmlReaderParser();
        $this->serializer = $serializer ?: new XmlWriterSerializer();
    }

    /**
     * Set the endpoint URI
     *
     * @param string $uri
     */
    public function setUri($uri)
    {
        if (!is_string($uri)) {
            throw InvalidArgumentException::expectedParameter(0, 'string', $uri);
        }

        $this->uri = $uri;
    }

    /**
     * Return endpoint URI
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Prepend default parameters that should always be prepended
     *
     * @param array $params
     */
    public function prependParams(array $params)
    {
        $this->prependParams = $params;
    }

    /**
     * Get default parameters that are always prepended
     *
     * @return array
     */
    public function getPrependParams()
    {
        return $this->prependParams;
    }

    /**
     * Append default parameters that should always be prepended
     *
     * @param array $params
     */
    public function appendParams(array $params)
    {
        $this->appendParams = $params;
    }

    /**
     * Get default parameters that are always appended
     *
     * @return array
     */
    public function getAppendParams()
    {
        return $this->appendParams;
    }

    public function stripInvalidXml($value)
    {
        $ret = "";
        $current = 0;
        if (empty($value)) 
        {
            return $ret;
        }
     
        $length = strlen($value);
        for ($i=0; $i < $length; $i++)
        {
            $current = ord($value[$i]);
            if (($current == 0x9) ||
                ($current == 0xA) ||
                ($current == 0xD) ||
                (($current >= 0x20) && ($current <= 0xD7FF)) ||
                (($current >= 0xE000) && ($current <= 0xFFFD)) ||
                (($current >= 0x10000) && ($current <= 0x10FFFF)))
            {
                $ret .= chr($current);
            }
            else
            {
                $ret .= " ";
            }
        }
        return $ret;
    }

    /** {@inheritdoc} */
    public function call($methodName, array $params = [])
    {
        if (!is_string($methodName)) {
            throw InvalidArgumentException::expectedParameter(0, 'string', $methodName);
        }

        $params = array_merge($this->prependParams, $params, $this->appendParams);
        $payload = $this->serializer->serialize($methodName, $params);
        $response = $this->transport->send($this->uri, $payload);

        $response = $this->stripInvalidXml($response);

        return $this->parser->parse($response);
    }

    /** {@inheritdoc} */
    public function multicall()
    {
        return new MulticallBuilder($this);
    }
}
